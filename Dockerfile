ARG IMAGEBASE=gitlab-registry.cern.ch/cms-cloud/cmssw-docker/cmssw_4_2_8-slc5_amd64_gcc434:latest
FROM ${IMAGEBASE}

ARG USERNAME=cmsusr
# Make this a build arg since we don't know the imagebase
# since it's defined before FROM
ARG CMSSW_VERSION=CMSSW_4_2_8

USER root

# If CMSSW_5_3_32 then add db
RUN if [[ $CMSSW_VERSION = "CMSSW_5_3_32"* ]]; \
    then \
    	 mkdir -p /cvmfs/cms-opendata-conddb.cern.ch \
    	 && export EOS_MGM_URL=root://eospublic.cern.ch \
	 && eos cp -r /eos/opendata/cms/conddb/selection-for-containers/cmssw_5_3_32/FT53_V21A_AN6_FULL/ /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && eos cp -d /eos/opendata/cms/conddb/selection-for-containers/cmssw_5_3_32/FT53_V21A_AN6_FULL_data_stripped.db /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && eos cp -r /eos/opendata/cms/conddb/selection-for-containers/cmssw_5_3_32/FT_53_LV5_AN1/ /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && eos cp -d /eos/opendata/cms/conddb/selection-for-containers/cmssw_5_3_32/FT_53_LV5_AN1_data_stripped.db /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && eos cp -r /eos/opendata/cms/conddb/selection-for-containers/cmssw_5_3_32/START53_LV6A1/ /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && eos cp -d /eos/opendata/cms/conddb/selection-for-containers/cmssw_5_3_32/START53_LV6A1_MC_stripped.db /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && eos cp -r /eos/opendata/cms/conddb/selection-for-containers/cmssw_5_3_32/START53_V27/ /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && eos cp -d /eos/opendata/cms/conddb/selection-for-containers/cmssw_5_3_32/START53_V27_MC_stripped.db /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && chown -R root:root /cvmfs/cms-opendata-conddb.cern.ch; \
    fi

# If CMSSW_7_6_7 then add db
RUN if [[ $CMSSW_VERSION = "CMSSW_7_6_7"* ]]; \
    then \
    	 mkdir -p /cvmfs/cms-opendata-conddb.cern.ch \
    	 && export EOS_MGM_URL=root://eospublic.cern.ch \
    	 && eos cp -d /eos/opendata/cms/conddb/76X_dataRun2_16Dec2015_v0.db /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && eos cp -d /eos/opendata/cms/conddb/76X_mcRun2_asymptotic_RunIIFall15DR76_v1.db /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && chown -R root:root /cvmfs/cms-opendata-conddb.cern.ch; \
    fi

# If CMSSW_10_6_30 then add db
RUN if [[ $CMSSW_VERSION = "CMSSW_10_6_30"* ]]; \
    then \
         mkdir -p /cvmfs/cms-opendata-conddb.cern.ch \
    	 && export EOS_MGM_URL=root://eospublic.cern.ch \
    	 && eos cp -d /eos/opendata/cms/conddb/106X_dataRun2_v37.db /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && eos cp -d /eos/opendata/cms/conddb/106X_mcRun2_asymptotic_v17.db /cvmfs/cms-opendata-conddb.cern.ch/ \
	 && chown -R root:root /cvmfs/cms-opendata-conddb.cern.ch; \
     fi

RUN git clone --depth 1 --single-branch https://github.com/novnc/noVNC.git /usr/local/novnc
RUN git clone --depth 1 --single-branch --branch v0.9.0 https://github.com/novnc/websockify.git /usr/local/novnc/utils/websockify

ADD vnc/vnc_utils.sh /usr/local/vnc_utils.sh

# install tigervnc server
RUN yum install -y tigervnc-server fluxbox xterm && \
    yum clean all && \
    rm -rf /tmp/.X*

RUN echo "source /usr/local/vnc_utils.sh" >> /etc/profile.d/bashrc.sh

USER ${USERNAME}
WORKDIR /code
RUN mkdir -p /home/${USERNAME}/.vnc
ADD --chown=cmsusr:cmsusr vnc/xstartup /home/${USERNAME}/.vnc/xstartup
ADD --chown=cmsusr:cmsusr vnc/passwd /home/${USERNAME}/.vnc/passwd

# For the Heavy Ion releases install and compile packages
COPY packages_HI_CMSSW_5_3_20.txt packages_HI_CMSSW_5_3_20.txt 
COPY packages_HI_CMSSW_7_5_8_patch3.txt packages_HI_CMSSW_7_5_8_patch3.txt

RUN if [[ $CMSSW_VERSION = "CMSSW_5_3_20"* ]]; \
    then \
         echo "Setting up ${CMSSW_VERSION}" \
         && source /cvmfs/cms.cern.ch/cmsset_default.sh \
         && scramv1 project CMSSW ${CMSSW_VERSION} \
         && cd ${CMSSW_VERSION}/src \
         && git init \
         && git remote add CmsHI https://github.com/CmsHI/cmssw.git \
         && git remote update \
	 && for package in $(cat ../../packages_HI_CMSSW_5_3_20.txt); \
	        do git checkout CmsHI/forest_${CMSSW_VERSION} -- ${package}; done \
         && scram b; \
    fi 

RUN if [[ $CMSSW_VERSION = "CMSSW_7_5_8_patch3"* ]]; \
    then \
         echo "Setting up ${CMSSW_VERSION}" \
         && source /cvmfs/cms.cern.ch/cmsset_default.sh \
         && scramv1 project CMSSW ${CMSSW_VERSION} \
         && cd ${CMSSW_VERSION}/src \
         && git init \
         && git remote add CmsHI https://github.com/CmsHI/cmssw.git \
         && git remote update \
	 && for package in $(cat ../../packages_HI_CMSSW_7_5_8_patch3.txt); \
	        do git checkout CmsHI/forest_${CMSSW_VERSION} -- ${package}; done \ 
         && scram b; \
    fi 

ENV GEOMETRY 1920x1080