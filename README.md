# CMSSW Docker images for Open Data tutorials

The images in this repository derive from https://gitlab.cern.ch/cms-cloud/cmssw-docker and have additional features for use with Open Data tutorials.

## Run

```bash
docker run -P -p 5901:5901 -p 6080:6080 ... [image]
```

where `...` is the rest of your usual `docker run` command.
(Optionally, a command to run inside the container like `/bin/bash` can be appended to this command.)

## Use VNC

To launch a VNC server, run this command: `start_vnc`

(For verbose output, use `start_vnc verbose`.)

If you have made changes in the `cmsusr` home directory, you will be asked to setup a password. It must be at least six characters in length.

Configuration Options:

* You can use the `GEOMETRY` environment variable to set the size of the VNC window. By default it is set to 1920x1080.
* If you run multiple VNC servers you can switch desktops by changing the `DISPLAY` environment variable like so: `export DISPLAY=myvnc:1`,
which will set the display of the remote machine to that of the VNC server.

At this point, you can connect to the VNC server with your favorite VNC viewer (TigerVNC, RealVNC, TightVNC, OSX built-in VNC viewer, etc.).
The following are the connection addresses:

* VNC viewer address: 127.0.0.1:5901
* Web browser URL: [http://127.0.0.1:6080/vnc.html](http://127.0.0.1:6080/vnc.html)

The connection password is `cms.cern`.

Note: On OSX you will need to go to System Preferences > Sharing and turn on "Screen Sharing" if using a VNC viewer, built-in or otherwise.
You will not need to do this if using the browser.

There are two additional helper functions:

* `stop_vnc`: kill all of the running vnc servers and the noVNC+WebSockify instance
* `clean_vnc`: run `stop_vnc` and clear all temporary files associated with the previous vnc servers

If you'd like more manual control you can use the following commands:

* `vncserver -list`: list the available VNC servers running on the remote machine.
* `vncserver -kill :1`: kill a currently running VNC server using. `:1` is the "X DISPLAY #".
* `pkill -9 -P <process>`: kill the noVNC+WebSockify process if you use the PID given when running `start_vnc` or when starting manually.

## Build

To build e.g. for CMSSW_7_6_7:

`docker build --build-arg="IMAGEBASE=gitlab-registry.cern.ch/cms-cloud/cmssw-docker/cmssw_7_6_7-slc6_amd64_gcc493" --build-arg="CMSSW_VERSION=CMSSW_7_6_7" .`

For more information and build-args and other options see the Dockerfile.